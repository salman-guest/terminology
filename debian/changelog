terminology (1.3.2-1) unstable; urgency=low

  * New upstream version 1.3.2.
  * Simplify watchfile by using @ARCHIVE_EXT@.

 -- Andreas Metzler <ametzler@debian.org>  Sun, 23 Dec 2018 06:42:47 +0100

terminology (1.3.1-1) unstable; urgency=high

  * New upstream release.
    + Fix for CVE-2018-20167: Disable special escape handling for unknown media
      types (Closes: #916630).
  * d/p/0002-Minor-manpage-improvements.patch: drop, applied upstream

 -- Ross Vandegrift <ross@kallisti.us>  Sun, 16 Dec 2018 10:28:36 -0800

terminology (1.3.0-1) unstable; urgency=medium

  * New upstream release.
  * Drop remnants of lz4 support, all support removed by upstream.
  * Bump Standards-Version to 4.2.1, no changes required.
  * d/p/0002-Minor-manpage-improvements.patch: add manpage for the ty*
    helpers, fix terminology(1) header. (Closes: #836300)

 -- Ross Vandegrift <ross@kallisti.us>  Sun, 25 Nov 2018 14:44:15 -0800

terminology (1.2.1-1) unstable; urgency=medium

  * New upstream release (Closes: #898384)

 -- Ross Vandegrift <ross@kallisti.us>  Mon, 14 May 2018 19:49:50 -0700

terminology (1.2.0-1) unstable; urgency=medium

  * New upstream release (Closes: #896930)

  [ Ross Vandegrift ]
  * d/gbp.conf: point gbp to debian/sid branch
  * d/control:
    + Bump Standards-Version to 4.1.4, no changes required.
    + Add branch name to Vcs-Git to fix vcswatch.
  * Upgrade to debhelper 11.
  * Convert to new meson build system.
  * d/patches:
    + fix-del-backspace-key.path: defuzz

  [ Andreas Metzler ]
  * refresh use-system-lz4.patch
  * update README.source with better gbp workflow.

 -- Ross Vandegrift <ross@kallisti.us>  Sat, 05 May 2018 10:37:44 -0700

terminology (1.1.1-3) unstable; urgency=low

  [ Ross Vandegrift ]
  * Update Vcs URLs for alioth -> salsa transition

  [ Andreas Metzler ]
  * Drop dead, unapplied patches (thanks, lintian!):
    cherrypick-geneet-fix.patch
    fix-manpage-invalid-newline-charater.patch
    fix-terminology-desktop-keywords.patch
  * Upload to unstable.
  * Add myself to uploaders.

 -- Andreas Metzler <ametzler@debian.org>  Mon, 16 Apr 2018 19:20:18 +0200

terminology (1.1.1-2) experimental; urgency=low

  * d/control:
    + drop Build-Dep on libelementary-dev, it's in libefl-all-dev now
    + Update Vcs links for terminology git
    + Bump Standards-Version to 4.1.2
    + Set Rules-Requires-Root to disable building as root
  * d/copyright: use https links for Format, Source
  * Use fake_home.sh from efl for build, edje_cc writes to $HOME (Closes:
    #884019)
  * d/watch: use https in watch url

 -- Ross Vandegrift <ross@kallisti.us>  Sun, 10 Dec 2017 12:52:34 -0800

terminology (1.1.1-1) experimental; urgency=medium

  * Team upload.
  * New upstream release
  * debian/copyright: update for recent contributions

 -- Ross Vandegrift <ross@kallisti.us>  Fri, 22 Sep 2017 20:06:26 -0700

terminology (1.1.0-1) experimental; urgency=medium

  * Team upload.
  * New upstream release
  * Update gbp.conf for experimental branch

 -- Ross Vandegrift <ross@kallisti.us>  Mon, 04 Sep 2017 21:03:31 -0700

terminology (1.0.0-1) experimental; urgency=medium

  * debian/gbp.conf: import & build upstream tarballs
  * debian/patches/use-system-lz4.patch: fixup, defuzz
  * debian/patches/fix-del-backspace-key.patch: fixup, defuzz
  * Build-Depend on EFL >= 1.18 and simplify package list (Closes: #848370)

 -- Ross Vandegrift <ross@kallisti.us>  Fri, 13 Jan 2017 13:50:18 -0500

terminology (0.9.1-1) unstable; urgency=high

  [ Ross Vandegrift ]
  * New upstream release
    - Fix for "CVE-2015-8971: Escape Sequence Command Execution
      vulnerability" (Closes: #843434)
  * fix-minus-signs-manpage.patch: drop patch, fixed upstream
  * use-system-lz4.patch: defuzz
  * fix-del-backspace-key.patch: defuzz
  * Provide x-terminal-emulator alternative (Closes: #774111)
  * debian/copyright: remove unused ltmain.sh paragraph
  * Add gbp.conf and notes on usage in README.source
  * Enable build hardening options
  * Suggest libemotion-players for media support (Closes: #773057, #766705)
  * Reformat package descriptions (Closes: #779494, #782082)
  * Use secure Vcs- URLs in debian/control
  * Bump Standards-Version to 3.9.8
  * New Maintainer.  Thanks to Anthony for original work. (Closes: #844244)
  * debian/gbp.conf: import & build upstream tarballs

  [ Nicolas Braud-Santoni ]
  * Normalize links and use HTTPS

 -- Ross Vandegrift <ross@kallisti.us>  Sun, 11 Dec 2016 12:25:35 -0500

terminology (0.7.0-1) unstable; urgency=medium

  * New upstream release
  * Removed patches included upstream, refreshed lz4 patch
  * Updated Standards-Version to 3.9.6

 -- Anthony F McInerney <afm404@gmail.com>  Thu, 16 Oct 2014 16:30:35 +0100

terminology (0.6.1-1) unstable; urgency=medium

  * Initial release (Closes: #718031)

 -- Anthony F McInerney <afm404@gmail.com>  Mon, 25 Aug 2014 14:23:53 +0100
